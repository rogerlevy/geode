\ internal actions
0 0 action deletion    ( - )  \
0 0 action instancing  ( - )  \

\ kludgy hooks
0 0 action ondelete  ( - )

\ common scripting actions
smudge start
0 0 action start   ( - )    \ actor script kickoff!
0 0 action physics ( - )    \
0 0 action draw    ( - )    \
0 0 action idle    ( - )    \ idle state
0 0 action die     ( - )    \ death state - play death animation etc
1 0 action hurt    ( u - )  \
1 0 action recover ( u - )  \
