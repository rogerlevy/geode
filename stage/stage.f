\ --------------------------------------------------------------------------------
\ instantiation

: one  ( prototype - entity )
  dup  >prototype {  instancing  } ;

: all  ( xt - )
  me >r s@ >r
  entities >items me!
  entities length 0 do
    extant @  en @ and  if dup >r execute r> then
    /geode-actorslot +to me
  loop
  drop
  r> s! r> me! ;

\ "each and every object" ... dumb name
: eaeo  ( xt - )
  me >r s@ >r
  entities >items me!
  entities length 0 do
    extant @ if dup >r execute r> then
    /geode-actorslot +to me
  loop
  drop
  r> s! r> me! ;

\ ------------------------------------------------------------------------------
\ deletion

: (actual-delete)  ( - )
  extant @ -exit  ondelete  deletion  extant off  en off  deleted off ;

: stage:deletions  ( - )
  [[ @ { (actual-delete) } ]] deletions each  deletions vacate ;


: delete  ( actor - )
  ?dup -exit  {
    deleted @ ?exit  deleted on  me deletions push
    deletions length geode-max-actors = if stage:deletions then
  } ;

\ : delete
\   be>  active @ -exit  ondelete @ execute  (delete) @ execute  en off  active off  ;

: clear-stage  ( - )
  [[ me delete ]] all  ; \ 0 nextpri ! ;

: copy  ( srcarray destarray - )
  locals| a2 a1 |
  0 a1 [] 0 a2 [] a1 length a1 .itemsize @ * a2 length a2 .itemsize @ * min cell/ imove ;

\ --------------------------------------------------------------------------------
\ Processing stage entities

: do-post  ( - )
  actor:post ;
: do-act  ( - )
  actor:act ;

: stage:sim  ( - )
  me
    ['] do-act all
    process-tweens
    stage:deletions
    ['] do-post all
    stage:deletions
  me! ;

\ : priority-sort  ( addr len )
\   [[ >priority @ swap >priority @ > ]] cellsort ;



\ ------------------------------------------------------------------------------
\ CLS

  variable (rgba)
  : unpack-rgba4f ( n-f:nnnn ) (rgba) ! (rgba) 4c@f ;
: cls  ( rgba8888 - )
  unpack-rgba4f 4sfparms al_clear_to_color ;

: do-draw  ( - )
  vis @ -exit  my xy 2i at  draw ;

: stage:render  ( - )
  backdrop @ cls
  me
    GL_GREATER 0e glAlphaFunc
    GL_ALPHA_TEST glEnable
    \ everything sortlist copy
    \ 0 sortlist []  geode-max-actors  priority-sort
    \ [[ @ me! ?draw ]] sortlist each
    ['] do-draw all
  me! ;

: goto-stage  ( - )
  ['] stage:sim is sim
  ['] stage:render is render
  ;

goto-stage

\ : .cast  [[ cr .name ]] all ;
