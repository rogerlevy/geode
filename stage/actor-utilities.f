: +hp  ( n - )  hp +!   hp @ 0 <= if die then ;
: -hp  ( n - )  negate +hp ;

:: hurt  -hp ;
:: recover  +hp ;
:: die   me delete ;

: ?die  ( - )
  hp @ 0 = if die r> drop exit then ;

: kill  ( actor - )
  { die } ;
: ?kill ( actor - flag )
  { hurt  hp @ 0 <=  } ;
: heal  ( n actor - )
  { recover } ;

: pos+  ( x. y. - x. y. )
  my xy 2+ ;

: from  ( entity x|. y|. - )
   2?p  rot { pos+ } at ;
