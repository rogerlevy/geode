%transform ~> %actor
  %vec2d inline vel   \
  true var: extant    \
  true var: en        \
  true var: vis       \
  var stays           \
  var flip            \
  var bmode           \ blend mode
  var hp              \
  var 'act            \
  var 'post           \
  var deleted         \
  var timer           \
  var lifetime        \
  var invuln          \
  var myid            \
  24 field myname     \
  %actor var: mytype  \
  var cflags          \ collision mask
  init: hitbox  0 ^ 0 ^ 100.0 ^ 100.0 ^      \ local - x,y,w,h
  1 var: tempdepth         \
  8 cells field tempdata   \
  var actorvt              \

  [[ @ actorvt @ ]]
    create actorvt-root  100 vtable,   actorvt-root actorvt !

  : actor:begetting  ( - )
    prototype me!  actorvt @ vbeget  dup actorvt !  actions:  pdef mytype ! ;
  ' actor:begetting begetting

  actorvt @ actions:
  fload actor-actions

  :: physics  ( - )
    vel xy pos +xy ;

endp


variable nextid
: *id  ( prototype - )
  body> >name count 1 /string <$  " -" $+
  nextid  dup @  dup myid !  (.) $+  $> myname place  ++ ;

: name?  ( - )  myname count type space ;

0 value whom  \ who (what actor) is calling ACTOR:ACT at the moment.
0 value osp

: (act)  ( code - )  code> 'act !  0 timer ! ;
: act   ( - <code> )  r> (act)  ;  \ i think this should only be used within PERFORM scripts, if at all.
: perform  ( n - )
  >r
  whom me = if  osp sp! r>
  else  1 tempdepth ! r> tempdata !
  then
  0 timer !  r> code> 'act ! ;

: ?sprange  ( n - n )
  dup -1 9 within not  if report" WARNING: Actor's local stack exceeded. " name? then
  8 min 0 max ;

: actor:act  ( - )
  whom >r
  me to whom
  a@ >r
  sp@ to osp
  sp@ tempdepth @ cells - sp!
  sp@ tempdata swap tempdepth @ imove
    'act @ execute
  osp sp@ cell+ - cell/ ?sprange tempdepth !
  sp@ tempdata tempdepth @ imove
  osp sp!
  r> a!
  r> to whom ;

: actor:post  ( - )
  physics  'post @ execute  timer ++  lifetime ++   invuln @ if invuln -- then ;

: init  ( class - )
  dup me initialize
  dup *id  mytype !  at@p my xy!
  start ;
