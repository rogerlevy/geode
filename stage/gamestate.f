
\ this version doesn't use a "free list" instead using roundrobin allocation, which
\  is better the stability of the engine when things are deleted and
\  then immediately instatiated, or referenced ... though, it's kind of an easy band-aid

2000 ?constant geode-max-actors

variable backdrop                                                              \
create entities    geode-max-actors /geode-actorslot array,                                \
create sortlist    geode-max-actors cell array,                                    \
create everything  geode-max-actors stack,  [[ everything push ]] entities each    \
create deletions   geode-max-actors stack,                                         \
variable #actives  \
variable nextent   \ offset

: stageobj:delete  ( - )
  #actives -- ;

: nextent+  ( - )
  nextent @ /geode-actorslot + [ geode-max-actors /geode-actorslot * 1 - ]# and nextent ! ;

: stageobj:instance  ( - actorslot )
  #actives @ geode-max-actors = abort" Error in STAGEOBJ:INSTANCE : Entity pool exhausted."
  begin
    entities >items  nextent @ +  dup .extant @ while  drop  nextent+
  repeat
  #actives ++ ;

%actor reopen
  prototype me!
  ' stageobj:delete  >:: deletion

  [[ ( prototype - instance )
    stageobj:instance  { init  me }  ]]  >:: instancing

endp
