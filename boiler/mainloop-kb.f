\ fixes modifier key bug
create ikbstate  /ALLEGRO_KEYBOARD_STATE /allot
: poll-keyboard  ( - )
  kbstate kblast /ALLEGRO_KEYBOARD_STATE move
  ikbstate kbstate /ALLEGRO_KEYBOARD_STATE move ;
: clear-keyboard  ( - )
  ikbstate /ALLEGRO_KEYBOARD_STATE erase  poll-keyboard ;
: bitmask  ( - )
  1 swap << ;
: setkey   ( bit - flag )
  32 /mod cells ikbstate cell+ + swap bitmask swap or! ;
: unsetkey ( bit - flag )
  32 /mod cells ikbstate cell+ + swap bitmask invert swap and! ;
: handle-kbstate-events  ( - )
  etype ALLEGRO_EVENT_KEY_DOWN = if  e .ALLEGRO_KEYBOARD_EVENT-keycode @ setkey   then
  etype ALLEGRO_EVENT_KEY_UP   = if  e .ALLEGRO_KEYBOARD_EVENT-keycode @ unsetkey then ;
