
: commandline  ( - )
  [[  0 0 al_set_new_window_position  init-game-window  ]] is warmup ;
: windowed    ( - )
  [[  init-game-window  ]] is warmup ;
: fullscreen  ( - )
  [[  init-game-fullscreen  display al_hide_mouse_cursor  ]] is warmup ;

[defined] program [if]
  : (program)  ( exename c - )
    " program " 2swap strjoin evaluate ;
  \ : iconpath  zhome zcount " \ico\appicon.ico" strjoin ;

  : debug  ( - <optional-name> )
    ['] development 'main !
    [[ !home  !root  warmup  load-assets  onloaded  cr  interactive  ]] 'starter !
    0 parse ?dup 0= if  drop  including -ext -path  then  +root
    cr  2dup type  (program) ;

  : deploy  ( - <optional-name> )
    [[  !home  !root  warmup  loader  +timer  onloaded  0 ExitProcess  ]] 'main !
    0 parse ?dup 0= if  drop  including -ext -path  then +root
    cr  2dup type  (program) ;
[then]

: test-script  ( - )
  onloaded$ place
  [[  onloaded$ count $fload  ]] is onloaded
  commandline  debug  ['] noop is onloaded ;

: releasebuild  ( xt - )
  ['] release-ok is ok  ( xt ) is onloaded  deploy ;
: devbuild  ( - )
  ['] dev-ok is ok  ['] noop is onloaded  debug ;

: filestem  ( - )
  including -path -ext ;

: bibuild  ( startxt - )
   dup " fullscreen  releasebuild bin\" <$  filestem $+  " _fullscreen" $+  $> evaluate
   " windowed 320 480 res 2! releasebuild bin\" <$  filestem $+  " _windowed"  $+  $> evaluate ;

: tribuild  ( startxt - )
   bibuild
   " commandline 320 480 res 2! devbuild bin\" <$  filestem $+  " _console" $+  $> evaluate ;
