variable rendertime                                \
variable simtime                                   \
defer dev?                                         \
order                                              \
defer ok                                           \
variable gameage                                   \
variable running                                   \
variable needredraw                                \
variable allowinput allowinput on                  \
variable update-on-events  update-on-events on     \
create e 256 allot                                 \
defer charkey-handler ( unicode -- )               \
  ' drop is charkey-handler                        \
defer wheel-handler ( n -- )                       \
  ' drop is wheel-handler                          \
defer extended-event-handler ( -- )                \
  ' noop is extended-event-handler                 \

: etype  ( - allegro-event-type )
  e @ ;

fload mainloop-kb

: (render)  ( - )
  ['] render exectime rendertime ! ;
: (sim)  ( - )
  ['] sim exectime simtime ! ;
: poll  ( - )
  poll-mouse  poll-joysticks  poll-keyboard ; \ poll-generic-inputs ;
: fs?  ( - flag )
  display al_get_display_flags ALLEGRO_FULLSCREEN_WINDOW and 0<> ;
: displaywh  ( - w h )
  display al_get_display_width display al_get_display_height ;
: desktopwh  ( - w h )
  al_get_num_display_modes 1 - pocket al_get_display_mode pocket 2@ ;

: w/f  ( - )
  display ALLEGRO_FULLSCREEN_WINDOW fs? if 0 else 1 then al_toggle_display_flag
    0= abort" could not switch to fullscreen" ;

: ?w/f  ( - )
  <enter> kpressed alt? and if w/f then ;

: ?f  ( - )
  <f> kpressed -exit
  display al_get_display_flags ALLEGRO_NOFRAME and 0<> not
  display ALLEGRO_NOFRAME rot al_toggle_display_flag drop
  display 0 0 al_set_window_position ;

: ?pause  ( - )
  dev? -exit pause ;
: ?poll  ( - )
  allowinput @ if  poll  ?w/f  then ;
: tick  ( - )
  gameage ++  ?pause  ?poll  (sim)  needredraw ++ ;

: get-loop  ( - sim-xt render-xt )
  ['] sim >body @   ['] render >body @ ;
: set-loop  ( sim-xt render-xt - )
  is render  is sim ;

: process-event  ( - )
  handle-kbstate-events

  etype ALLEGRO_EVENT_DISPLAY_RESIZE = if
    display al_acknowledge_resize
    e .ALLEGRO_DISPLAY_EVENT-width 2@ res 2!
  then

  etype ALLEGRO_EVENT_DISPLAY_CLOSE = if
    0 ExitProcess
  then

  etype ALLEGRO_EVENT_KEY_CHAR = if
    e .ALLEGRO_KEYBOARD_EVENT-unichar @ charkey-handler
  then

  etype ALLEGRO_EVENT_MOUSE_AXES = if
    e .ALLEGRO_MOUSE_EVENT-dz @ ?dup if wheel-handler then
  then

  update-on-events @ if

    etype case
      ALLEGRO_EVENT_TIMER of  ?pause  ( update-sounds ) exit endof
    endcase

    \ report" Event"
    extended-event-handler

    etype ALLEGRO_EVENT_MOUSE_AXES      =
    etype ALLEGRO_EVENT_MOUSE_BUTTON_DOWN = or
    etype ALLEGRO_EVENT_MOUSE_BUTTON_UP  = or
    etype ALLEGRO_EVENT_KEY_DOWN       = or
    etype ALLEGRO_EVENT_KEY_CHAR       = or
    etype ALLEGRO_EVENT_KEY_UP         = or
    etype ALLEGRO_EVENT_DISPLAY_SWITCH_IN = or
    etype ALLEGRO_EVENT_DISPLAY_SWITCH_OUT = or
    if  ?poll  (sim)  (render)  show-frame   then

    etype ALLEGRO_EVENT_DISPLAY_SWITCH_OUT =
    if  clear-keyboard  then

    etype ALLEGRO_EVENT_DISPLAY_EXPOSE   =
    etype ALLEGRO_EVENT_DISPLAY_FOUND    = or
    etype ALLEGRO_EVENT_DISPLAY_RESIZE   = or
    etype ALLEGRO_EVENT_DISPLAY_ORIENTATION = or
    if (render) show-frame  then

  else

    etype ALLEGRO_EVENT_TIMER = if  tick ( update-sounds )
    else
      \ report" Event"
      extended-event-handler
    then

    needredraw @ if
      eventq al_is_event_queue_empty needredraw @ 4 >= or if
        (render) show-frame needredraw off
      then
    then
  then
;

: event-pump  ( - )
  eventq e al_wait_for_event  process-event ;

\ this is the main loop (development)
: dev-ok  ( - )
  poll-keyboard \ important to fix escape key bug
  running on
  >gfx +timer begin event-pump
  <escape> kpressed shift? and until
  clear-keyboard
  -timer >ide running off ;

[[ ['] ok >body @ ['] dev-ok = ]] is dev?

: release-ok  ( - )
  poll-keyboard \ important to fix escape key bug
  [[ running on
    >gfx +timer begin event-pump
    <f4> kpressed alt? and until
  ]] catch  -timer >ide running off  throw ;

' dev-ok is ok

: ?ok  ( - )
  running @ ?exit ok ;
: ?bye  ( - )
  dev? not if 0 ExitProcess else -timer -1 abort" Intentional program termination." then ;
: gofor  ( n - )
  +timer gameage @ + >r begin event-pump gameage @ r@ >= until r> drop -timer ;
: interlude  ( simxt renderxt frames - )
  -rot get-loop 2>r set-loop gofor 2r> set-loop ;

\ exposed
: event  ( - allegro-event )
  e ;
