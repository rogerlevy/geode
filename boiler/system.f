create windowtitle 256 allot                   \
create onloaded$  256 allot                    \
defer render    ' noop is render               \
defer warmup    ' noop is warmup               \
defer onloaded  ' noop is onloaded             \
defer sim       ' noop is sim                  \
defer loader  [[ load-assets ]] is loader      \

: !windowtitle  ( - )
  display windowtitle count z$ al_set_window_title ;
: titlebar  ( adr c - )
  windowtitle place  !windowtitle ;
" Dev" titlebar

: init-other-stuff  ( - )  !windowtitle  init-input  ;
: init-game-window  ( - )  init-allegro  init-other-stuff ;
: init-game-fullscreen  ( - )  init-allegro-fullscreen  init-other-stuff ;

\ extend prompt to update the display when enter is pressed (for testing graphics)
' prompt >body @ value (prompt)
:prune   ?prune if (prompt) is prompt report" Old Prompt restored" then ;

: green  ( - )  7 attribute ;
: magenta  ( - )  5 attribute ;

: (game-prompt)  ( - )
  .stack  state @ 0= if  >gfx  s@ >R  render  R> s!  show-frame  >ide  magenta  ."  ok"  normal  then  cr ;
: game-prompt  ( - )
  ['] (game-prompt) is prompt  report" Set Game Prompt " ;

: (gfx-prompt)  ( - )
  .stack  state @ 0= if  >gfx  show-frame  >ide  green  ."  ok"  normal  then  cr ;
: gfx-prompt  ( - )
  ['] (gfx-prompt) is prompt  report" Set Graphics Prompt " ;

game-prompt
