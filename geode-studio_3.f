package GAME-WL end-package
package AA-WL end-package
variable side

: switch-sides   side @ -order  dup side !  +order  definitions ;

only forth definitions
fload geode-boiler             \ main loop ( OK etc )

: game    game-wl switch-sides  update-on-events off ;
: aa      aa-wl   switch-sides  update-on-events on ;
: empty   empty  only forth  side @  dup +order  aa-wl = update-on-events ! ;

aa
fload apprentice               \ apprentice framework for widgets and interactive development

game
push-order         \ leave the GAME order effective after leaving this file
fload geode-stage
