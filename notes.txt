
domain-switching mechanism
    GAME
    STUDIO
    SIDE   - variable that holds either GAMEWL or STUDIOWL
    EMPTY  - extended to +ORDER whatever is in SIDE


todo:
[ ] scripting.f


-------------------------------------------


crystals found inside geodes:
calcite
pyrite
kaolinite
sphalerite
millerite
barite
celestite
dolomite
limonite
smithsonite
opal
chalcedony
macrocrystalline quartz


-----------------------

module idea (based on Richard's advice on how to deal with importing actors from different "engines")

\ bullet collision example
post  [[

  [module shootemup  pwr @ ( target ) { hurt }  module]
  [module rpg        pwr @ ( target ) { hurt }  module]

]] [[ ( filter ) ]] collide ;



-----------------------

some game-specific actions taken out

0 0 action walk    ( - )  \
smudge run
0 0 action run     ( - )  \
0 0 action fall    ( - )  \
1 0 action chase   ( actor - )  \
1 0 action avoid   ( actor - )  \
2 0 action attack  ( attack# power - )  \ attack# is an enum of attack types. totally up to the user how to define them.
0 0 action intro   ( - )  \
3 0 action shoot   ( vx. vy. projectile-actor - )  \
0 0 action ?shoot  ( - )  \
1 0 action face    ( actor - )  \
0 0 action wake    ( - )  \
0 0 action slumber ( - )  \

\ create -dv   0 , 1.0 , 0 , -1.0 , 1.0 , 0 , -1.0 , 0 ,
\ : -dirv   direction @ 2 cells * dv + 2@ speed @ dup 2p* ;
\ : -travel  -dirv vel 2! ;

: forward   ( n. - )
  speed ! travel ;
: backward  ( n. - )
  negate speed ! travel ;

:: walk   travel ;
\ :: idle   0 speed !  travel ;

: walkl  ( - )
  left direction !  walk ;
: walkr  ( - )
  right direction !  walk ;

:: shoot   me 0 0 from one { vel xy! } ;

create dv  \  direction vectors
  0 , -1.0 , 0 , 1.0 , -1.0 , 0 , 1.0 , 0 ,

: dirv   ( - x. y. )
  direction @ 2 cells * dv + 2@ speed @ dup 2p* ;
: travel  ( - )
  dirv vel xy! ;
