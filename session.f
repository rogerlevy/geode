\ {nodoc} tell the documentation progress monitor tool to ignore this file
true constant config-sys-sf
true constant config-arch-x86
true constant config-os-windows

include ../newbase/index-sf

2000 constant geode-max-actors
512 constant /geode-actorslot

fload geode-studio_3

\ while documenting, this saves a step of having to load the progress monitor tool manually.
include ../newbase/internal/docprogress.f
